package com.projectswg.launcher.game;

import com.projectswg.common.concurrency.Delay;
import com.projectswg.common.control.IntentManager;
import com.projectswg.common.control.PrimaryManager;
import com.projectswg.common.debug.Log;
import com.projectswg.common.debug.ThreadPrinter;
import com.projectswg.common.debug.log_wrapper.FileLogWrapper;
import com.projectswg.launcher.game.launcher.LauncherConnection;
import com.projectswg.launcher.game.process.ProcessManager;
import com.projectswg.launcher.game.server.ServerConnection;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;

public class GameLauncher extends PrimaryManager {
	
	public static void main(String [] args) throws IOException {
		String dateTime = new SimpleDateFormat("YYYYMMdd-HHmmss").format(System.currentTimeMillis());
		File tmp = Files.createTempFile("pswg-game-"+dateTime+"-", ".log").toFile();
		Log.addWrapper(new FileLogWrapper(tmp));
		int port = parsePortArgument(args);
		IntentManager intentManager = new IntentManager(Runtime.getRuntime().availableProcessors());
		IntentManager.setInstance(intentManager);
		intentManager.initialize();
		GameLauncher launcher = new GameLauncher(port);
		try {
			Log.i("Initializing...");
			if (launcher.initialize()) {
				Log.i("Initialized.");
				Log.i("Starting...");
				if (launcher.start()) {
					Log.i("Started.");
					while (launcher.isOperational()) {
						if (Delay.sleepMilli(100))
							break;
					}
				} else {
					Log.w("Failed to start!");
				}
				Log.i("Stopping...");
				launcher.stop();
				Log.i("Stopped.");
			} else {
				Log.w("Failed to initialize!");
			}
			Log.i("Terminating...");
			launcher.terminate();
			Log.i("Terminated.");
		} catch (Throwable t) {
			Log.e(t);
		}
		intentManager.terminate();
		ThreadPrinter.printActiveThreads();
		Log.i("Done.");
		System.exit(0);
	}
	
	public GameLauncher(int port) {
		addChildService(new ProcessManager());
		addChildService(new ServerConnection());
		addChildService(new LauncherConnection(port));
	}
	
	private static int parsePortArgument(String [] args) {
		if (args.length < 1) {
			Log.e("Invalid number of arguments. Expected port.");
			throw new IllegalArgumentException("Expected port");
		}
		if (args.length > 1)
			Log.w("Invalid number of arguments. Continuing with first argument");
		try {
			return Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			Log.e("Invalid port number: %s", args[0]);
			throw new IllegalArgumentException("Invalid port");
		}
	}
	
}

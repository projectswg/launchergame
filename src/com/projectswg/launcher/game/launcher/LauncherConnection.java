/***********************************************************************************
 * Copyright (c) 2017 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Holocore.                                                  *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Holocore is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Holocore is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Holocore.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.launcher.game.launcher;

import com.projectswg.common.concurrency.PswgBasicThread;
import com.projectswg.common.control.Service;
import com.projectswg.common.debug.Assert;
import com.projectswg.common.debug.Log;
import com.projectswg.common.network.TCPSocket;
import com.projectswg.launcher.game.intents.ReceivedLauncherPacketIntent;
import com.projectswg.launcher.game.intents.SendLauncherPacketIntent;
import com.projectswg.launcher.game.launcher.packets.*;
import me.joshlarson.json.JSONException;
import me.joshlarson.json.JSONInputStream;
import me.joshlarson.json.JSONObject;
import me.joshlarson.json.JSONOutputStream;

import java.io.*;
import java.net.Inet6Address;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Class that provides communication to the launcher
 */
public class LauncherConnection extends Service {
	
	private final CommunicationListener listener;
	private final AtomicReference<JSONOutputStream> outputStream;
	private final TCPSocket socket;
	
	public LauncherConnection(int port) {
		this.listener = new CommunicationListener();
		this.socket = new TCPSocket(new InetSocketAddress(Inet6Address.getLoopbackAddress(), port), 4096);
		this.outputStream = new AtomicReference<>(null);
	}
	
	public boolean start() {
		try (Scanner scanner = new Scanner(System.in)) {
			String token = scanner.nextLine();
			socket.connect();
			OutputStream os = new OutputStream() {
				public void write(int b) {						socket.send(new byte[]{(byte) b}); }
				public void write(byte[] b) {					socket.send(b); }
				public void write(byte[] b, int off, int len) {	socket.send(b, off, len); }
			};
			
			PipedInputStream pipedInputStream = new PipedInputStream();
			PipedOutputStream pipedOutputStream = createOutputPipe(pipedInputStream);
			socket.setCallback(new TCPSocket.TCPSocketCallback() {
				public void onConnected(TCPSocket socket) { }
				public void onDisconnected(TCPSocket socket) {
					try {
						pipedOutputStream.close();
					} catch (IOException e) {
						Log.e(e);
					}
				}
				public void onIncomingData(TCPSocket socket, byte[] data) {
					try {
						pipedOutputStream.write(data);
					} catch (IOException e) {
						Log.e(e);
					}
				}
			});
			
			this.outputStream.set(new JSONOutputStream(os));
			this.listener.setInputStream(pipedInputStream);
			this.listener.start();
			Log.i("Sending token: %s", token);
			socket.send(token.getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			Log.e("Failed to connect to Launcher TCP server: %s", e.getMessage());
			return false;
		}
		
		registerForIntent(SendLauncherPacketIntent.class, this::handleSendLauncherPacketIntent);
		return super.start();
	}
	
	public boolean stop() {
		socket.disconnect();
		listener.stop();
		return super.stop();
	}
	
	private void handleSendLauncherPacketIntent(SendLauncherPacketIntent slpi) {
		try {
			JSONOutputStream outputStream = this.outputStream.get();
			Objects.requireNonNull(outputStream);
			outputStream.writeObject(slpi.getPacket().getJSON());
			outputStream.flush();
		} catch (IOException e) {
			Log.e(e);
		}
	}
	
	private static PipedOutputStream createOutputPipe(PipedInputStream inputStream) {
		try {
			return new PipedOutputStream(inputStream);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	private static class CommunicationListener {
		
		private final PswgBasicThread thread;
		private final AtomicReference<InputStream> inputStream;
		private final AtomicBoolean running;
		
		public CommunicationListener() {
			this.thread = new PswgBasicThread("communication-listener", this::run);
			this.inputStream = new AtomicReference<>(null);
			this.running = new AtomicBoolean(false);
		}
		
		public void start() {
			Assert.test(!running.getAndSet(true));
			thread.start();
		}
		
		public void stop() {
			Assert.test(running.getAndSet(false));
			try {
				System.in.close();
			} catch (IOException e) {
				Log.w("Failed to close input stream!");
			}
			thread.stop(true);
		}
		
		public void setInputStream(InputStream inputStream) {
			this.inputStream.set(inputStream);
		}
		
		private void run() {
			try (JSONInputStream inputStream = new JSONInputStream(this.inputStream.get())) {
				JSONObject obj;
				Log.d("Listening...");
				while ((obj = inputStream.readObject()) != null) {
					handlePacket(obj);
				}
			} catch (JSONException | IOException e) {
				Log.e("LauncherConnection - %s in process(): %s", e.getClass().getSimpleName(), e.getMessage());
			}
			Log.d("Done listening.");
		}
		
		private static void handlePacket(JSONObject obj) {
			String typeStr = obj.getString("type");
			if (typeStr == null) {
				Log.w("Invalid JSON received: %s", obj.toString(true));
				return;
			}
			GamePacketType type = GamePacketType.getPacketType(typeStr);
			Log.d("Received packet: %s", type);
			GamePacket packet = parsePacket(type, obj);
			if (packet == null)
				return;
			ReceivedLauncherPacketIntent.broadcast(type, packet);
		}
		
		private static GamePacket parsePacket(GamePacketType type, JSONObject obj) {
			switch (type) {
				case SERVER_INFORMATION:			return new ServerInformationPacket(obj);
				case SERVER_REQUEST_OUTPUT:			return new ServerRequestOutputPacket(obj);
				case SERVER_REQUEST_STOP:			return new ServerRequestStopPacket(obj);
				default:
					Log.w("Received invalid packet: %s", type);
					Log.w("    Object: %s", obj.toString(true));
					return null;
			}
		}
		
	}
	
}

/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Holocore.                                                  *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Holocore is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Holocore is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Holocore.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.launcher.game.launcher.packets;

import com.projectswg.common.data.EnumLookup;

public enum GamePacketType {
	// BOTH
	UNKNOWN,
	
	// Server
	SERVER_INFORMATION,
	SERVER_REQUEST_OUTPUT,
	SERVER_REQUEST_STOP,
	SERVER_START_OUTPUT_UPDATES,
	SERVER_STOP_OUTPUT_UPDATES,
	
	// CLIENT
	CLIENT_CONNECTION_UPDATE,
	CLIENT_OUTPUT;
	
	private static final EnumLookup<String, GamePacketType> NAME_LOOKUP = new EnumLookup<>(GamePacketType.class, GamePacketType::name);
	
	public static GamePacketType getPacketType(String name) {
		return NAME_LOOKUP.getEnum(name, GamePacketType.UNKNOWN);
	}
}

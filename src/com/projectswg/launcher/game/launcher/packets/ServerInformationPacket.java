/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Holocore.                                                  *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Holocore is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Holocore is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Holocore.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.launcher.game.launcher.packets;

import me.joshlarson.json.JSONObject;

import java.net.InetSocketAddress;

public class ServerInformationPacket extends GamePacket {
	
	private final InetSocketAddress addr;
	private final String user;
	private final String pass;
	private final String winePath;
	private final String wineArgs;
	private final String wineVars;
	private final String swgDir;
	
	private ServerInformationPacket(InetSocketAddress addr, String user, String pass, String winePath, String wineArgs, String wineVars, String swgDir) {
		this.addr = addr;
		this.user = user;
		this.pass = pass;
		this.winePath = winePath;
		this.wineArgs = wineArgs;
		this.wineVars = wineVars;
		this.swgDir = swgDir;
	}
	
	public ServerInformationPacket(JSONObject obj) {
		JSONObject data = obj.getObject("data");
		{
			JSONObject connection = data.getObject("connection");
			this.addr = new InetSocketAddress(connection.getString("ip"), connection.getInt("port"));
			this.user = connection.getString("user");
			this.pass = connection.getString("pass");
		}
		{
			JSONObject wine = data.getObject("wine");
			this.winePath = wine.getString("winePath");
			this.wineArgs = wine.getString("wineArgs");
			this.wineVars = wine.getString("wineVars");
		}
		this.swgDir = data.getString("swgDir");
	}
	
	public JSONObject getJSON() {
		JSONObject data = new JSONObject();
		data.putLayered("connection.ip", addr.getAddress().getHostAddress());
		data.putLayered("connection.port", addr.getPort());
		data.putLayered("connection.user", user);
		data.putLayered("connection.pass", pass);
		data.putLayered("wine.winePath", winePath);
		data.putLayered("wine.wineArgs", wineArgs);
		data.putLayered("wine.wineVars", wineVars);
		data.put("swgDir", swgDir);
		return createJSON(GamePacketType.SERVER_INFORMATION, data);
	}
	
	public InetSocketAddress getAddress() {
		return addr;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getPass() {
		return pass;
	}
	
	public String getWinePath() {
		return winePath;
	}
	
	public String getWineArgs() {
		return wineArgs;
	}
	
	public String getWineVars() {
		return wineVars;
	}
	
	public String getSwgDir() {
		return swgDir;
	}
	
	public static ServerInformationPacket.Builder builder() {
		return new ServerInformationPacket.Builder();
	}
	
	public static class Builder {
		
		private InetSocketAddress	addr		= null;
		private String				user		= null;
		private String				pass		= null;
		private String				winePath	= null;
		private String				wineArgs	= null;
		private String				wineVars	= null;
		private String				swgDir		= null;
		
		Builder() {
			
		}
		
		public Builder setAddr(InetSocketAddress addr) {
			this.addr = addr;
			return this;
		}
		
		public Builder setUser(String user) {
			this.user = user;
			return this;
		}
		
		public Builder setPass(String pass) {
			this.pass = pass;
			return this;
		}
		
		public Builder setWinePath(String winePath) {
			this.winePath = winePath;
			return this;
		}
		
		public Builder setWineArgs(String wineArgs) {
			this.wineArgs = wineArgs;
			return this;
		}
		
		public Builder setWineVars(String wineVars) {
			this.wineVars = wineVars;
			return this;
		}
		
		public Builder setSwgDir(String swgDir) {
			this.swgDir = swgDir;
			return this;
		}
		
		public ServerInformationPacket build() {
			return new ServerInformationPacket(addr, user, pass, winePath, wineArgs, wineVars, swgDir);
		}
		
	}
	
}

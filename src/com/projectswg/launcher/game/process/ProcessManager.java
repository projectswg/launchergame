/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Holocore.                                                  *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Holocore is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Holocore is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Holocore.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.launcher.game.process;

import com.projectswg.common.control.Service;
import com.projectswg.common.debug.Log;
import com.projectswg.launcher.game.intents.ReceivedLauncherPacketIntent;
import com.projectswg.launcher.game.intents.SendLauncherPacketIntent;
import com.projectswg.launcher.game.intents.StartGameIntent;
import com.projectswg.launcher.game.launcher.packets.ClientOutputPacket;
import com.projectswg.launcher.game.launcher.packets.GamePacketType;
import com.projectswg.launcher.game.launcher.packets.ServerInformationPacket;
import com.projectswg.launcher.game.utilities.ExecUtilities;
import com.projectswg.launcher.game.utilities.ExecUtilities.PSWGProcess;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class ProcessManager extends Service {
	
	private final AtomicReference<PSWGProcess> process;
	private final AtomicBoolean operational;
	
	public ProcessManager() {
		this.process = new AtomicReference<>(null);
		this.operational = new AtomicBoolean(true);
	}
	
	@Override
	public boolean initialize() {
		registerForIntent(StartGameIntent.class, this::handleStartGameIntent);
		registerForIntent(ReceivedLauncherPacketIntent.class, this::handleReceivedLauncherPacketIntent);
		return super.initialize();
	}
	
	@Override
	public boolean isOperational() {
		PSWGProcess process = this.process.get();
		return operational.get() && (process == null || process.getRunning());
	}
	
	@Override
	public boolean terminate() {
		PSWGProcess process = this.process.get();
		if (process != null) {
			process.stop();
			SendLauncherPacketIntent.broadcast(new ClientOutputPacket(process.getOutput()));
		}
		return super.terminate();
	}
	
	private void handleReceivedLauncherPacketIntent(ReceivedLauncherPacketIntent rlpi) {
		if (rlpi.getType() != GamePacketType.SERVER_REQUEST_OUTPUT)
			return;
		PSWGProcess process = this.process.get();
		if (process == null) {
			SendLauncherPacketIntent.broadcast(new ClientOutputPacket(""));
		} else {
			SendLauncherPacketIntent.broadcast(new ClientOutputPacket(process.getOutput()));
		}
	}
	
	private void handleStartGameIntent(StartGameIntent sgi) {
		ServerInformationPacket info = sgi.getInfo();
		String username = info.getUser();
		Log.i("Starting game...");
		String[] args = new String[] {
				"--",
				"-s",
				"Station",
				"subscriptionFeatures=1",
				"gameFeatures=34374193",
				"-s",
				"ClientGame",
				"loginServerPort0=" + sgi.getConnection().getLoginPort(),
				"loginServerAddress0=127.0.0.1",
				"loginClientID=" + username,
				"autoConnectToLoginServer=" + !username.isEmpty(),
				"logReportFatals=true",
				"logStderr=true",
				"0fd345d9=true",
				};
		if (this.process.get() != null) {
			Log.w("Failed to start! Already started.");
			return;
		}
		
		PSWGProcess process = ExecUtilities.exec(new File(info.getWinePath()), new File(info.getSwgDir(), "SwgClient_r.exe"), args);
		if (process == null) {
			Log.e("Failed to start PSWGProcess!");
			operational.set(false);
			return;
		}
		if (!this.process.compareAndSet(null, process)) {
			process.stop();
		}
	}
	
}

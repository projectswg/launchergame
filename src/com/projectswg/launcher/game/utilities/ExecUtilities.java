/***********************************************************************************
 * Copyright (c) 2017 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Holocore.                                                  *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Holocore is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Holocore is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Holocore.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                                 *
 ***********************************************************************************/

/*
 *
 * This file is part of ProjectSWG Launchpad.
 *
 * ProjectSWG Launchpad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * ProjectSWG Launchpad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with ProjectSWG Launchpad.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.projectswg.launcher.game.utilities;

import com.projectswg.common.debug.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecUtilities {
	
	public static boolean isWindows() {
		return System.getProperty("os.name").startsWith("Windows");
	}
	
	public static PSWGProcess exec(File wine, File program, String ... args) {
		try {
			String[] processString;
			if (isWindows()) {
				processString = new String[] {program.getCanonicalPath()};
			} else {
				processString = new String[] {wine.getCanonicalPath(), program.getCanonicalPath()};
			}
			
			String [] combined = new String[processString.length + args.length];
			System.arraycopy(processString, 0, combined, 0, processString.length);
			System.arraycopy(args, 0, combined, processString.length, args.length);
			ProcessBuilder pb = new ProcessBuilder(combined);
			pb.redirectErrorStream(true);
			pb.directory(program.getParentFile());
			pb.environment().put("WINEDEBUG", "-all");
			return new PSWGProcess(pb.start());
		} catch (IOException e) {
			Log.e(e);
		}
		return null;
	}
	
	public static class PSWGProcess {
		
		private static final AtomicInteger PID = new AtomicInteger(0);
		private final StringBuilder output;
		private final AtomicBoolean running;
		private final Process process;
		private final Thread readingThread;
		
		public PSWGProcess(Process process) {
			this.output = new StringBuilder();
			this.running = new AtomicBoolean(false);
			this.process = process;
			this.readingThread = new Thread(this::readingLoop, "process-"+PID.getAndIncrement()+"-ingester");
			this.readingThread.start();
		}
		
		public String getOutput() {
			return output.toString();
		}
		
		public boolean getRunning() {
			return running.get();
		}
		
		public void stop() {
			try {
				process.getInputStream().close();
			} catch (IOException e) {
				Log.w("Failed to close process input stream!");
			}
			process.destroy();
			try {
				process.waitFor(1, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				Log.e("PSWGProcess - interrupted while waiting for completion");
			}
			readingThread.interrupt();
		}
		
		private void readingLoop() {
			try {
				running.set(true);
				InputStream is = process.getInputStream();
				int b;
				while ((b = is.read()) != -1) {
					if (b == '\r')
						continue;
					output.append((char) b);
				}
			} catch (InterruptedIOException e) {
				// Ignored
			} catch (Throwable t) {
				Log.e(t);
			} finally {
				running.set(false);
			}
		}
		
	}
	
}
